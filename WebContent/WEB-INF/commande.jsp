<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.2/normalize.min.css" />
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css"
	rel="stylesheet" />
	 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<link href="assets/css/style.css" rel="stylesheet" />
<link href="assets/css/animate.css" rel="stylesheet" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
	href="https://fonts.googleapis.com/css?family=Kaushan+Script|Lato|Roboto|Bungee+Shade&display=swap"
	rel="stylesheet">
<title>JSPizzeria</title>
</head>
<body>

	<div class="container-fluid ">


		
		<!-- NAVIGATION -->
		<div class="row d-flex justify-content-end">
			<nav class="navbar navbar-expand-lg col-lg-12 navbar-custom-auth">
			
	<div class="navbar-collapse collapse d-flex justify-content-end" id="navbarCustom">
				<ul class="navbar-nav navbar-nav-auth d-flex flex-row-reverse">
					<li class="nav-item-auth "><a class="nav-link nav-link-auth "
						href="Home.jsp" target="_self">ACCUEIL</a></li>
					<li class="nav-item-auth "><a class="nav-link nav-link-auth "
						href="pizzas.jsp" target="_self">NOS PIZZAS</a></li>
					<c:if test="${sessionScope['active']}">
						<li class="nav-item-auth">
						<form action="account" method="post" >
						<button type="submit" class="nav-link nav-link-auth  btn btn-link" >MON PROFIL  <i class="bi bi-person"></i>  </button>
							</form>
							</li>
							<li class="nav-item-auth">
							<form action="panier" method="post" >
								<button type="submit" class="nav-link nav-link-auth  btn btn-link">MON PANIER  <i class="bi-cart"></i>  </button>
							</form>
						</li>
					</c:if>
				</ul>

			</div>
			<div>
			<a href="Home.jsp" class="navbar-brand navbar-brand-auth"
				target="_self">JSPizzeria</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarCustom">
				<i class="fa fa-bars fa-lg py-1 text-white"></i>
			</button></div>

			</nav>

		</div>
	</div>

	<div class="container manualPlacement">
		<div class="row d-flex justify-content-center">
			<h1>Ma commande</h1>
		</div>
		<hr class="hrlite">
	</div>


	<div class="container ">
		<div class="row" id="listeArticleCommande">

			<table class="table-sm table-hover col-12">
				<thead>
					<tr>
						<th scope="col">nom</th>
						<th scope="col">ref</th>
						<th scope="col">prix</th>
						<th scope="col">quantite</th>
						<th scope="col">Total ligne</th>
					</tr>
				</thead>
				<tbody>

					<c:forEach items="${sessionScope['cart']}" var="cartElem"
						varStatus="rowCommande">
						<form action="commande" method="post">
							<tr class="d-table-row  ">
								<th scope="row">${cartElem.key.nom }</th>
								<td><input class="no-borde" readonly size="2" type="text"
									name="pizzaRef" value="${cartElem.key.ref}"></td>
								<td>${cartElem.key.tarif }</td>

								<td>
									<div class="col-4">
										<label for="" class="form-label"></label> <input
											type="submit" class="btn btn-outline-danger"
											data-bs-toggle="button" aria-pressed="false"
											autocomplete="off" name="button" value="-"> <input
											readonly size="2" type="text" name="quantite"
											value="${cartElem.value}"> <input type="submit"
											class="btn btn-outline-success" data-bs-toggle="button"
											aria-pressed="false" autocomplete="off" name="button"
											value="+">

									</div>
								</td>
								<td>
									<div class="d-flex justify-content-between">
										<fmt:parseNumber var="quantite" integerOnly="false"
											type="number" value="${cartElem.value}" />
										<fmt:parseNumber var="tarif" integerOnly="false" type="number"
											value="${cartElem.key.tarif}" />

										${quantite*tarif} euros <input type="submit"
											class=" btn btn-close" aria-label="Close" name="button"
											value="">

									</div>
								</td>
							</tr>
						</form>
					</c:forEach>

				</tbody>

			</table>



		</div>
		<div class="row">
			<table class="table table-bordered col-4">
				<thead>
					<tr>
						<th scope="col" class="col-3">TOTAL FACTURE</th>
						<th scope="col" class="col-1">valeur total :
							${sessionScope['totalPanier']}</th>
					</tr>
				</thead>
			</table>
		</div>
		<div class="row">
			<form class="form-inline col-6" action="commande" method="post">
				<button class="btn btn-outline-danger" type="submit" name="button"
					value="Vider le panier">Vider le panier</button>
			</form>
			<form class="form-inline col-6 d-flex justify-content-end" action="validateOrder" method="post">
				<button class="btn btn-outline-success" type="submit" name="button"
					value="Vider le panier">Valider la commande</button>
			</form>
		</div>
	</div>


</body>
</html>