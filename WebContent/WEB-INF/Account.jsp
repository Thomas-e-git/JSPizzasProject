<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.2/normalize.min.css" />
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css"
	rel="stylesheet" />
<link href="assets/css/style.css" rel="stylesheet" />
<link href="assets/css/animate.css" rel="stylesheet" />
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
	href="https://fonts.googleapis.com/css?family=Kaushan+Script|Lato|Roboto|Bungee+Shade&display=swap"
	rel="stylesheet">
<title>JSPizzeria</title>
</head>
<body>
	<div class="container-fluid ">
		<!-- NAVIGATION -->
		<div class="row d-flex justify-content-end">
			<nav class="navbar navbar-expand-lg col-lg-12 navbar-custom-auth">

			<div class="navbar-collapse collapse d-flex justify-content-end"
				id="navbarCustom">
				<ul class="navbar-nav navbar-nav-auth d-flex flex-row-reverse">
					<li class="nav-item-auth "><a class="nav-link nav-link-auth "
						href="Home.jsp" target="_self">ACCUEIL</a></li>
					<li class="nav-item-auth "><a class="nav-link nav-link-auth "
						href="pizzas.jsp" target="_self">NOS PIZZAS</a></li>
					<c:if test="${sessionScope['active']}">
						<li class="nav-item-auth">
							<form action="account" method="post">
								<button type="submit"
									class="nav-link nav-link-auth  btn btn-link">
									MON PROFIL <i class="bi bi-person"></i>
								</button>
							</form>
						</li>
						<li class="nav-item-auth">
							<form action="panier" method="post">
								<button type="submit"
									class="nav-link nav-link-auth  btn btn-link">
									MON PANIER <i class="bi-cart"></i>
								</button>
							</form>
						</li>
					</c:if>
				</ul>

			</div>
			<div>
				<a href="Home.jsp" class="navbar-brand navbar-brand-auth"
					target="_self">JSPizzeria</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarCustom">
					<i class="fa fa-bars fa-lg py-1 text-white"></i>
				</button>
			</div>

			</nav>

		</div>
	</div>



	<div class="container manualPlacement infoUser">
		<div class="row d-flex justify-content-center">
			<h1>Mon profil</h1>
			<h3>Mr/Mme ${sessionScope['user'].nom}</h3>
			<div class="col-12">
				<div class="profil">
					<hr>
					<h3>NOM : ${sessionScope['user'].nom}</h3>
					<h3>PRENOM : ${sessionScope['user'].prenom}</h3>
					<hr class="hrlite">
					<h3>ADRESSE : ${sessionScope['user'].complement.getAdresse()}
					</h3>
					<h3>TELEPHONE :
						${sessionScope['user'].complement.getTelephone()}</h3>
					<h3>INSTRUCTION :
						${sessionScope['user'].complement.getInstructions()}</h3>
					<hr class="hr">
					<h3>HISTORIQUE DE VOS COMMANDES :</h3>

				</div>

			</div>
		</div>

	</div>


	<div class="container ">
		<div class="row">
			<div class="accordion accordion-flush" id="accordionFlushExample">
				<c:forEach items="${sessionScope['recap']}" var="recapElem"
					varStatus="count">
					<div class="accordion-item">
						<h2 class="accordion-header" id="flush-headingOne">
							<button class="accordion-button collapsed" type="button"
								data-bs-toggle="collapse"
								data-bs-target="#test${recapElem.key.id}" aria-expanded="false"
								aria-controls="flush-collapseOne">
								Votre commande du ${recapElem.key.date } d'un montant total de :
								<span class="fw-bold">&nbsp; ${recapElem.key.prixtotal}
									euros</span>
							</button>
						</h2>
						<div id="test${recapElem.key.id}"
							class="accordion-collapse collapse"
							aria-labelledby="flush-headingOne"
							data-bs-parent="#accordionFlushExample">
							<div class="accordion-body">
								<table class="table-sm table-hover col-12">
									<thead>
										<tr>
											<th scope="col">nom</th>
											<th scope="col">prix</th>
											<th scope="col">quantite</th>
											<th scope="col">Total ligne</th>
										</tr>
									</thead>
									<c:forEach items="${recapElem.value}" var="valueElem"
										varStatus="count">
										<tr class="d-table-row ">
											<td>${valueElem.key.nom }</td>
											<td>${valueElem.key.tarif }euros</td>
											<td>${valueElem.value }</td>
											<td>${valueElem.key.tarif * valueElem.value }euros</td>
										</tr>
									</c:forEach>
								</table>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
</body>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"
	integrity="sha384-eMNCOe7tC1doHpGoWe/6oMVemdAVTMs2xqW4mwXrXsW0L84Iytr2wi5v2QjrP/xp"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js"
	integrity="sha384-cn7l7gDp0eyniUwwAZgrzD06kc/tftFf19TOAs2zVinnD/C7E91j9yyk5//jjpt/"
	crossorigin="anonymous"></script>
</html>