<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.2/normalize.min.css" />
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css"
	rel="stylesheet" />
	 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<link href="assets/css/style.css" rel="stylesheet" />
<link href="assets/css/animate.css" rel="stylesheet" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
	href="https://fonts.googleapis.com/css?family=Kaushan+Script|Lato|Roboto|Bungee+Shade&display=swap"
	rel="stylesheet">
<title>JSPizzeria</title>
</head>
<body>


	<div class="container-fluid ">


		<!-- NAVIGATION -->
		<div class="row   " >
			<nav class="navbar navbar-expand-lg col-lg-12 navbar-custom">

			<div class="navbar-collapse collapse justify-content-end d-flex" id="navbarCustom">
				<ul class="navbar-nav d-flex flex-row-reverse">
					<li class="nav-item "><a class="nav-link active"
						href="Home.jsp" target="_self">ACCUEIL</a></li>
					<li class="nav-item "><a class="nav-link active"
						href="pizzas.jsp" target="_self">NOS PIZZAS</a></li>
					<c:if test="${sessionScope['active']}">
						<li class="nav-item">
						<form action="account" method="post" >
						<button type="submit" class="nav-link nav-link-auth  btn btn-link">MON PROFIL  <i class="bi bi-person"></i>  </button>
							</form>
							</li>
						<li class="nav-item">
							<form action="panier" method="post" >
							<button type="submit" class="nav-link nav-link-auth  btn btn-link">MON PANIER  <i class="bi-cart"></i>  </button>
							</form>
						</li>
					</c:if>
				</ul>

			</div>
			<div>
				<a href="Home.jsp" class="navbar-brand" target="_self">JSPizzeria</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarCustom">
				<i class="fa fa-bars fa-lg py-1 text-white"></i>
			</button>
			</div>
			
		
			</nav>

		</div>

	</div>


	<!-- ACCUEIL CENTRAL IMAGES ANIMEES -->
	<div class="row home col-12 ">

		<div class="carousel-img col-lg-12 "></div>
		<div class="row  d-flex ">

			<div
				class="firstpart col-6 d-flex justify-content-center align-items-center">

				<img class="logoplacement"  src="img/logopizzeria.png" alt="logo JSPizzeria" id="logoid"> </img>
			</div>



			<div class=" col-6 d-flex justify-content-center align-items-center">

				<div id="connectPanel" class="card col-8 d-flex justify">
					<img class="card-img-top" src="holder.js/100x180/" alt="">
					<div class="card-body">

						<c:if test="${sessionScope['active'] == false}">
							<h4 class="card-title">Connection</h4>

							<hr class="hrlite">

							<p class="card-text">
							<form class="row g-3 auth" novalidate method="post" action="auth">

								<div class="col-md-4">
									<label for="validationCustom010" class="form-label">Identifiant</label>
									<input name="id" type="text" class="form-control"
										id="validationCustom01" placeholder="Saisir identifiant"
										required />
									<div class="valid-feedback">Champs valide</div>
									<div class="invalid-feedback">Veuillez saisir votre
										identifiant.</div>
								</div>
								<div class="col-md-4">
									<label for="validationCustom0100" class="form-label">Mot
										de passe</label> <input name="pass" type="password"
										class="form-control" id="validationCustom01"
										placeholder="Saisir identifiant" required />
									<div class="valid-feedback">Champs valide</div>
									<div class="invalid-feedback">Veuillez saisir votre mot
										de passe.</div>
								</div>

								<div class="col-12">
									<button class="btn btn-outline-primary" type="submit"
										onsubmit="">Se connecter</button>
								</div>
								<c:if test="${error != null}">
									<p class="text-danger h5">${error}</p>
								</c:if>
							</form>
							<form class="row g-3 needs-validation" novalidate method="post" action="register">


								<p>
									<i> Pas encore de compte ? <b> <a
											data-bs-toggle="collapse" href="#collapseExample"
											role="button" aria-expanded="false"
											aria-controls="collapseExample"> Cr�er un nouveau compte
										</a>
									</b>
									</i>
								</p>
								<div class="collapse" id="collapseExample">
									<hr class="hr">
									<div class="row">


										<div class="col-md-4">
											<label for="validationCustom01" class="form-label">Choisir
												un identifiant</label> <input name="id" type="text"
												class="form-control" id="validationCustom01"
												placeholder="Saisir identifiant" required />
											<div class="valid-feedback">Champs valide</div>
											<div class="invalid-feedback">Choisir un identifiant.</div>
										</div>
										<div class="col-md-4">
											<label for="validationCustom01" class="form-label">Choisir
												un mot de passe</label> <input name="pass" type="password"
												class="form-control" id="validationCustom01"
												placeholder="Saisir identifiant" required />
											<div class="valid-feedback">Champs valide</div>
											<div class="invalid-feedback">Veuillez saisir votre mot
												de passe.</div>
										</div>


										<div class="col-md-4">
											<label for="validationCustom05" class="form-label">Entrez
												votre nom.</label> <input name="nom" type="text"
												class="form-control" id="validationCustom05"
												placeholder="Nom" required />
											<div class="valid-feedback">Champs valide</div>
											<div class="invalid-feedback">Veuillez saisir votre
												nom.</div>
										</div>
										<div class="col-md-4">
											<label for="validationCustom01" class="form-label">Entrez
												votre prenom.</label> <input name="prenom" type="text"
												class="form-control" id="validationCustom06"
												placeholder="Prenom" required />
											<div class="valid-feedback">Champs valide</div>
											<div class="invalid-feedback">Veuillez saisir votre
												prenom.</div>
										</div>



										<div class="col-md-8">
											<label for="validationCustom03" class="form-label">Adresse</label>
											<input name="adresse" type="text" class="form-control"
												id="validationCustom03" required placeholder="Adresse"/>
											<div class="invalid-feedback">Veuillez saisir votre
												adresse</div>
										</div>
										<div class="col-md-3">
											<label for="validationCustom04" class="form-label">t�l�phone</label>
											<input name="telephone" type="text-area" class="form-control"
												id="validationCustom04" required placeholder="t�l�phone"/>
											<div class="invalid-feedback">Veuillez saisir numero de
												t�l�phone</div>
										</div>
										<div class="col-md-9">
											<label  class="form-label">Instructions</label>
											<textarea rows="3" name="instructions" type="text" class="form-control"
												id="validationCustom04" required placeholder="Instructions de livraison"/></textarea>
										</div>




										<div class="col-12">
											<div class="form-check">
												<input class="form-check-input" type="checkbox" value=""
													id="invalidCheck" required /> <label
													class="form-check-label" for="invalidCheck">
													J'accepte les termes et conditions d'utilisateur </label>
												<div class="invalid-feedback">Veuillez accepter les
													conditions d'utilisation</div>


											</div>

											<div class="col-12">
												<button class="btn btn-outline-primary" type="submit"
													onsubmit="">
													S'inscrire <i class="fas fas fa-user-plus  pl-1 "></i>
												</button>
											</div>


										</div>
									</div>
							</form>

							</p>
						</c:if>
						<c:if test="${sessionScope['active']}">
							<p class="text-success h2 text-center mx-auto ">Bienvenue
								${sessionScope['user'].prenom}</p>
							
							<div class="row m-3">
							<form action="logout" method="post" class="col-5 d-flex justify-content-end">
								<input class="btn btn-outline-danger" type="submit"
									value="Deconnexion">
							</form>
							<a href="http://localhost:8080/JSPizzasProject/pizzas.jsp" class="col-5"><button
									class=" btn btn-outline-success">Commencer la commande</button></a>
							</div>
						</c:if>
					</div>
				</div>
			</div>

		</div>

	</div>

	</div>








	<svg xmlns="http://www.w3.org/2000/svg" style="display: none;"> <symbol
		id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
	<path
		d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
	</symbol> </svg>
	



	<script type="text/javascript" src="js/formValidation.js"></script>
	<script type="text/javascript" src="js/modal.js"></script>
	<script
		src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"
		integrity="sha384-eMNCOe7tC1doHpGoWe/6oMVemdAVTMs2xqW4mwXrXsW0L84Iytr2wi5v2QjrP/xp"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js"
		integrity="sha384-cn7l7gDp0eyniUwwAZgrzD06kc/tftFf19TOAs2zVinnD/C7E91j9yyk5//jjpt/"
		crossorigin="anonymous"></script>



</body>
</html>