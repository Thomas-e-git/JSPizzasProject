<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.2/normalize.min.css" />
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css"
	rel="stylesheet" />
	 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<link href="assets/css/style.css" rel="stylesheet" />
<link href="assets/css/animate.css" rel="stylesheet" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
	href="https://fonts.googleapis.com/css?family=Kaushan+Script|Lato|Roboto|Bungee+Shade&display=swap"
	rel="stylesheet">
<title>JSPizzeria</title>
</head>
<body>

	<div class="container-fluid ">


		<!-- NAVIGATION -->
		<div class="row d-flex justify-content-end">
			<nav class="navbar navbar-expand-lg col-lg-12 navbar-custom-auth">
			
	<div class="navbar-collapse collapse d-flex justify-content-end" id="navbarCustom">
				<ul class="navbar-nav navbar-nav-auth d-flex flex-row-reverse">
					<li class="nav-item-auth "><a class="nav-link nav-link-auth "
						href="Home.jsp" target="_self">ACCUEIL</a></li>
					<li class="nav-item-auth "><a class="nav-link nav-link-auth "
						href="pizzas.jsp" target="_self">NOS PIZZAS</a></li>
					<c:if test="${sessionScope['active']}">
						<li class="nav-item-auth">
						<form action="account" method="post" >
						<button type="submit" class="nav-link nav-link-auth  btn btn-link" >MON PROFIL  <i class="bi bi-person"></i>  </button>
							</form>
							</li>
							<li class="nav-item-auth">
							<form action="panier" method="post" >
								<button type="submit" class="nav-link nav-link-auth  btn btn-link">MON PANIER  <i class="bi-cart"></i>  </button>
							</form>
						</li>
					</c:if>
				</ul>

			</div>
			<div>
			<a href="Home.jsp" class="navbar-brand navbar-brand-auth"
				target="_self">JSPizzeria</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarCustom">
				<i class="fa fa-bars fa-lg py-1 text-white"></i>
			</button></div>

			</nav>

		</div>
	</div>


	<div class="container manualPlacement listePizzaContainer">
		<div class="row col-7 text-center  mx-auto p-2 mb-5">
			<img src="img/logopizzeria.png" class="w-50 mx-auto">
			<c:if test="${info != null}">
				<p class="text-primary h3">${info}</p>
			</c:if>
		</div>
		<div class="row d-flex justify-content-between align-items-start"
			id="listePizzas">
			<c:forEach items="${applicationScope['listePizzas']}" var="pizza"
				varStatus="status">
				<div id="" class="card m-1" style="width: 18rem;">
					<img src="${pizza.image}" class="card-img-top card-image" alt="..."
						style="border: 18rem;" />
					<div class="card-body">
						<h5 class="card-title">${pizza.nom}</h5>
						<hr class="hrlite" />
						<p class="card-text">${pizza.description}</p>
						<hr class="hrlite" />
						<p class="card-text">Prix : ${pizza.tarif}</p>
						<hr class="hrlite" />

						<div class="input-group mb-3">
							
								<form action="pizzas" class="row col-12" method="post">
									<input name="quantite" type="number" min="0"
										class="form-control " placeholder="Quantite" />
									<button name="indice" value="${status.count - 1}"
										class="btn btn-outline-success " type="submit">
										Ajouter au panier</button>
								</form>
							
						</div>
					</div>
				</div>
				
			</c:forEach>




		</div>
	</div>


</body>
</html>