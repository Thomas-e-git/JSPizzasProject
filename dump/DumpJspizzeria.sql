-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: jspizzeria
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `ref` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  `description` varchar(500) NOT NULL,
  `tarif` double NOT NULL,
  `image` varchar(45) NOT NULL,
  PRIMARY KEY (`ref`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'Margarita','Base tomate, Mozzarella, Basilic, Huile d\'olive',8,'img/margarita.jpg'),(2,'La fromagère','Base crème, Mozarella, Roquefort, Chèvre, Emmental',11,'img/fromagere.jpg'),(3,'Indienne','Base crème, Poulet mariné, Curry, Mozzarella, Oignons, Champignons',12,'img/indienne.jpg'),(4,'Nordique','Base crème, Saumon, Mozarella, Jus de citron, Aneth',13,'img/nordique.jpg'),(5,'La Basquaise','Base tomate, Chorizo, Mozarella, Oignons, Chèvre, Piment doux',12,'img/basquaise.jpg'),(6,'Orientale','Base tomate, Merquez, Poivrons, Oignons, Mozzarella',12,'img/orientale.jpg'),(7,'Reine','Base tomate, Jambon, Champignons, Mozzarella',11,'img/reine.jpg'),(8,'Landaise','Base tomate, Magret de canard, Foie gras de canard, Gésiers de canard, Mozzarella',15,'img/landaise.jpg'),(9,'Parma','Base tromate, Jambon de Parme, Parmesan, Roquette',11,'img/parma.jpg'),(10,'Spéciale','Base crème, Poitrine fumée, Mozarella, Saint Nectaire, Oignons, Champignons',13,'img/speciale.jpg'),(11,'Veggie','Base crème, Mozarella, Aubergine, Emmental, Oignons, Champignons',11,'img/veggie.jpg'),(12,'La Saisonnière ','Base tomate, Poulet, Mozarella, Champignons, Tomates cerises, Oignons, Courgettes',13,'img/saison.jpg');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `id` varchar(45) NOT NULL,
  `pass` varchar(45) NOT NULL,
  `nom` varchar(45) NOT NULL,
  `prenom` varchar(45) NOT NULL,
  `adresse` varchar(45) NOT NULL,
  `telephone` varchar(45) NOT NULL,
  `instructions` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES ('dudu','dudu','dudu','dudu','dudu','dudu','dudu'),('titi','titi','titi','titi','25 rue exemple','0654651228','test'),('toto','toto','toto','toto','12 rue exemple','06 05 35 64 14','Batiment F, appt 65, code 1235');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commandes`
--

DROP TABLE IF EXISTS `commandes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commandes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idClient` varchar(45) NOT NULL,
  `date` varchar(45) NOT NULL,
  `prixtotal` double NOT NULL,
  `infos` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commandes`
--

LOCK TABLES `commandes` WRITE;
/*!40000 ALTER TABLE `commandes` DISABLE KEYS */;
/*!40000 ALTER TABLE `commandes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-18 11:11:41
