package model;

public class Client {

	private String id;
	private String pass;
	private String nom;
	private String prenom;
	private Complement complement;
	
	
	public Client() {
		super();
	}

	public Client(String id, String pass, String nom, String prenom, String adresse, String telephone, String instructions) {
		this.id = id;
		this.pass = pass;
		this.nom = nom;
		this.prenom = prenom;
		this.complement = new Complement(adresse, telephone, instructions);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Complement getComplement() {
		return complement;
	}

	public void setComplement(Complement complement) {
		this.complement = complement;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", pass=" + pass + ", nom=" + nom + ", prenom=" + prenom + ", complement="
				+ complement + "]";
	}
	
	
	
}
