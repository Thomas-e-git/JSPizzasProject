package model;

public class Article {

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ref;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Article other = (Article) obj;
		if (ref != other.ref)
			return false;
		return true;
	}

	private int ref;
	private String nom;
	private String description;
	private double tarif;
	private String image;
	
	public Article() {
		super();
	}

	public Article(int ref, String nom, String description, double tarif, String image) {
		super();
		this.ref = ref;
		this.nom = nom;
		this.description = description;
		this.tarif = tarif;
		this.image = image;
	}

	public int getRef() {
		return ref;
	}

	public void setRef(int ref) {
		this.ref = ref;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getTarif() {
		return tarif;
	}

	public void setTarif(double tarif) {
		this.tarif = tarif;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "Article [ref=" + ref + ", nom=" + nom + ", description=" + description + ", tarif=" + tarif + ", image="
				+ image + "]";
	}
	
	

	
}
