package model;

public class Order {
	private int id;
	private String idClient;
	private String date;
	private double prixtotal;
	private String infos;
	
	public Order() {
		super();
	}

	public Order(int id, String idClient, String date, double prixtotal, String infos) {
		super();
		this.id = id;
		this.idClient = idClient;
		this.date = date;
		this.prixtotal = prixtotal;
		this.infos = infos;
	}
	
	

	public Order(String idClient, String date, double prixtotal, String infos) {
		super();
		this.idClient = idClient;
		this.date = date;
		this.prixtotal = prixtotal;
		this.infos = infos;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIdClient() {
		return idClient;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public double getPrixtotal() {
		return prixtotal;
	}

	public void setPrixtotal(double prixtotal) {
		this.prixtotal = prixtotal;
	}

	public String getInfos() {
		return infos;
	}

	public void setInfos(String infos) {
		this.infos = infos;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", idClient=" + idClient + ", date=" + date + ", prixtotal=" + prixtotal + ", infos="
				+ infos + "]";
	}
	
	
}
