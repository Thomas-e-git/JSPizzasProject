package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Order;

public class DaoOrder {

      public Order create(Order d) throws SQLException, SQLException, ClassNotFoundException {

		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jspizzeria", "root", "root");
		String sql = "insert into commandes (idClient, date, prixtotal, infos) values (?,?,?,?)";
		// public Order(int id, String idClient, String date, double prixtotal,
		// String infos)

		PreparedStatement ps = conn.prepareStatement(sql);
		
		ps.setString(1, d.getIdClient());
		ps.setString(2, d.getDate());
		ps.setDouble(3, d.getPrixtotal());
		ps.setString(4, d.getInfos());
		ps.executeUpdate();

		return d;
	}

	public ArrayList<Order> findAll() throws SQLException, SQLException, ClassNotFoundException

	{
		Order o = new Order();
		ArrayList<Order> liste = new ArrayList<Order>();
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jspizzeria", "root", "root");
		// System.out.println("connexion ok ");

		String sql = "select * from  articles ";
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);

		// public Article(int ref, String nom, String description, double tarif,
		// String image)
		while (rs.next())

		{
			// public Order(int id, int idClient, String date, double prixtotal,
			// String infos)
			o.setId(rs.getInt("id"));
			o.setIdClient(rs.getString("IdClient"));
			o.setDate(rs.getString("date"));
			o.setPrixtotal(rs.getDouble("prixtotal"));
			o.setInfos(rs.getString("infos"));
			liste.add(o);

		}
		return liste;
	}

	public ArrayList<Order> findByClient(String idClient) throws SQLException, SQLException, ClassNotFoundException

	{
		ArrayList<Order> liste = new ArrayList<Order>();
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jspizzeria", "root", "root");
		String sql = "select * from commandes where idClient = '"+ idClient + "'";

		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);
		while (rs.next())
		{
			Order o = new Order(); 
			o.setId(rs.getInt("id"));
			o.setIdClient(rs.getString("idClient"));
			o.setDate(rs.getString("date"));
			o.setPrixtotal(rs.getDouble("prixtotal"));
			o.setInfos(rs.getString("infos"));
			liste.add(o);
		}
		return liste;

	}

}
