package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Article;

public class DaoArticle {
	public ArrayList<Article> findAll() throws SQLException, SQLException, ClassNotFoundException

	{

		ArrayList<Article> liste = new ArrayList<Article>();
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jspizzeria", "root", "root");
		// System.out.println("connexion ok ");

		String sql = "select * from  articles ";
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);

		// public Article(int ref, String nom, String description, double tarif,
		// String image)
		while (rs.next())

		{
			Article a = new Article();
			a.setRef(rs.getInt("ref"));
			a.setNom(rs.getString("nom"));
			a.setDescription(rs.getString("description"));
			a.setTarif(rs.getDouble("tarif"));
			a.setImage(rs.getString("image"));
			liste.add(a);

		}
		return liste;
	}

	public Article findById(int ref ) throws SQLException, SQLException, ClassNotFoundException
	{
		Article a = new Article();
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jspizzeria", "root", "root");
	    String sql = "select * from articles where ref = "+ ref ; 
	    Statement st = conn.createStatement();
	    ResultSet rs = st.executeQuery(sql);
	    while (rs.next())

		{
			
			a.setRef(rs.getInt("ref"));
			a.setNom(rs.getString("nom"));
			a.setDescription(rs.getString("description"));
			a.setTarif(rs.getDouble("tarif"));
			a.setImage(rs.getString("image"));
			

		}
	    return a ;
		
	}
	public ArrayList<Article>FindAllByOrder(String  info )throws SQLException, SQLException, ClassNotFoundException
	{
	ArrayList<Article> liste = new ArrayList<Article>();
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jspizzeria", "root", "root");
	 String sql = "select * from articles  , commandes where articles.infos  = commandes.infos and commandes.infos =  "+ info ; 
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);

		// public Article(int ref, String nom, String description, double tarif,
		// String image)
		while (rs.next())

		{
			Article a = new Article();
			a.setRef(rs.getInt("ref"));
			a.setNom(rs.getString("nom"));
			a.setDescription(rs.getString("description"));
			a.setTarif(rs.getDouble("tarif"));
			a.setImage(rs.getString("image"));
			liste.add(a);

		}
		return liste;
	}
	
	 
}
