package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Client;

public class DaoClient {

	public Client findByIdPass(String id, String pass)
			throws SQLException, SQLException, ClassNotFoundException {
		Client c = null;
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jspizzeria", "root", "root");
		String sql = "select * from clients where id  = ? and pass = ?";

		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, id);
		ps.setString(2, pass);
		ResultSet rs = ps.executeQuery();

		if (rs.next()) {
			c = new Client(rs.getString("id"),
					rs.getString("pass"),
					rs.getString("nom"),
					rs.getString("prenom"),
					rs.getString("adresse"),
					rs.getString("telephone"),
					rs.getString("instructions"));
		}
		conn.close();
		return c;
	}
	
	public Client create(Client c)
			throws SQLException, SQLException, ClassNotFoundException {
		
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jspizzeria", "root", "root");
		String sql = "insert into clients values (?,?,?,?,?,?,?)";

		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, c.getId());
		ps.setString(2, c.getPass());
		ps.setString(3, c.getNom());
		ps.setString(4, c.getPrenom());
		ps.setString(5, c.getComplement().getAdresse());
		ps.setString(6, c.getComplement().getTelephone());
		ps.setString(7, c.getComplement().getInstructions());
		ps.executeUpdate();

		return c;
	}
	
}
