package srv;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Article;


/**
 * Servlet implementation class Pizzas
 */
@WebServlet("/pizzas")
public class Pizzas extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Pizzas() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Boolean active = (Boolean) request.getSession().getAttribute("active");
		if (!active) {
			request.setAttribute("error", "Veuillez vous connecter ou creer un compte pour commencer votre commande");
			request.getRequestDispatcher("Home.jsp").forward(request, response);
		} else {
			ArrayList<Article> listePizzas = (ArrayList) request.getServletContext().getAttribute("listePizzas");
			int indice = Integer.parseInt(request.getParameter("indice"));
			Article pizza = listePizzas.get(indice);
			String quantiteStr = request.getParameter("quantite");
			Map<Article, Integer> cart = new HashMap<Article, Integer>();
			cart = (Map<Article, Integer>) request.getSession().getAttribute("cart");
			if (quantiteStr != "" && Integer.parseInt(quantiteStr) > 0) {
				int quantite = Integer.parseInt(quantiteStr);
				cart.put(pizza, quantite);
				request.getSession().setAttribute("cart", cart);
				request.setAttribute("info",quantite + " pizzas " + pizza.getNom() + " ajout� � la commande");
			} else {
				request.setAttribute("info", "Entrez une quantit� valide");
			}
			request.getRequestDispatcher("pizzas.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
