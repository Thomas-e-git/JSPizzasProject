package srv;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoArticle;
import model.Article;

/**
 * Servlet implementation class Commande
 */
@WebServlet("/commande")
public class Commande extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Commande() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Map<Article, Integer> cart = new HashMap<Article, Integer>();
		cart = (Map<Article, Integer>) request.getSession().getAttribute("cart");
		int pizzaRef = 0;
		int quantite = 0;
		String button = request.getParameter("button");
		DaoArticle daoArticle = new DaoArticle();
		if ( request.getParameter("pizzaRef") != null)
			pizzaRef = Integer.parseInt(request.getParameter("pizzaRef"));
		Article pizza = new Article();
		if ( request.getParameter("quantite") != null)
			quantite = Integer.parseInt(request.getParameter("quantite"));
		double totalPanier = 0;
		
		try {
			pizza = daoArticle.findById(pizzaRef);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if ("+".equals(button)) {
			int actualQuantity = cart.get(pizza);
			cart.replace(pizza, actualQuantity, ++actualQuantity);
			request.getSession().setAttribute("cart",cart);
		} else if ("-".equals(button) && quantite > 0) {
			int actualQuantity = cart.get(pizza);
			cart.replace(pizza, actualQuantity, --actualQuantity);
			request.getSession().setAttribute("cart",cart);
		} else if ("".equals(button)) {
			cart.remove(pizza);
		} else if ("Vider le panier".equals(button)) {
			cart.clear();
		}
		for(Map.Entry<Article, Integer> entry : cart.entrySet()) {
		    Article key = entry.getKey();
		    Integer value = entry.getValue();
		    totalPanier += key.getTarif()*value;
		}
		request.getSession().setAttribute("totalPanier",totalPanier);
		request.getRequestDispatcher("WEB-INF/commande.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
