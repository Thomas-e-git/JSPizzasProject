package srv;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoOrder;
import model.Article;
import model.Client;
import model.Order;

/**
 * Servlet implementation class ValidateOrder
 */
@WebServlet("/validateOrder")
public class ValidateOrder extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ValidateOrder() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Client client = (Client)request.getSession().getAttribute("user");
		Map<Article, Integer> cart = new HashMap<Article, Integer>();
		cart = (Map<Article, Integer>) request.getSession().getAttribute("cart");
		String infos = "";
		for(Map.Entry<Article, Integer> entry : cart.entrySet()) {
		    Article key = entry.getKey();
		    Integer value = entry.getValue();
		    infos += key.getRef() + "-" + value + ";";
		}
		DaoOrder daoOrder = new DaoOrder();
		Double prixTotal = (Double)request.getSession().getAttribute("totalPanier");
		Order order = new Order(client.getId(),LocalDate.now().toString(),prixTotal,infos);
		try {
			daoOrder.create(order);
			request.getSession().setAttribute("order", order);
			request.getRequestDispatcher("WEB-INF/ordervalidation.jsp").forward(request, response);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
