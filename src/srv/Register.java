package srv;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DaoClient;
import model.Client;

/**
 * Servlet implementation class NewClient
 */
@WebServlet("/register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DaoClient daoClient = new DaoClient();
		HttpSession session = request.getSession();
		Client user = null;
		Client client = new Client(
				request.getParameter("id"),
				request.getParameter("pass"),
				request.getParameter("nom"),
				request.getParameter("prenom"),
				request.getParameter("adresse"),
				request.getParameter("telephone"),
				request.getParameter("instructions")
				);
		try {
			daoClient.create(client);
			user = daoClient.findByIdPass(client.getId(), client.getPass());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (user != null) {
			session.setAttribute("user", user);
			
			session.setAttribute("active", true);
		} else {
			request.setAttribute("error", "La cr�ation de compte n'a pas fonctionn�");
			
		}
		request.getRequestDispatcher("Home.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
