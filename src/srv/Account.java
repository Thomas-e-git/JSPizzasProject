package srv;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoArticle;
import dao.DaoOrder;
import model.Article;
import model.Client;
import model.Order;

/**
 * Servlet implementation class Account
 */
@WebServlet("/account")
public class Account extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Account() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Client user = (Client) request.getSession().getAttribute("user");
		String userId = user.getId();
		DaoOrder daoOrder = new DaoOrder();
		DaoArticle daoArticle = new DaoArticle();
		ArrayList<Order> listeCommandes = new ArrayList<Order>();
		try {
			listeCommandes = daoOrder.findByClient(userId);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		Map<Order, HashMap<Article, Integer>> recap = new HashMap<Order, HashMap<Article, Integer>>();
		for (Order commande : listeCommandes) {
			String str = commande.getInfos();
			String[] keyValuePairs = str.split(";");
			Map<Article, Integer> map = new HashMap<>();
			for (String pair : keyValuePairs) {
				String[] entry = pair.split("-");
				Article pizza = new Article();
				try {
					pizza = daoArticle.findById(Integer.parseInt(entry[0].trim()));
				} catch (NumberFormatException | ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				map.put(pizza, Integer.parseInt(entry[1].trim()));
			}
			recap.put(commande, (HashMap<Article, Integer>) map);
		}
		request.getSession().setAttribute("listeCommandes", listeCommandes);
		request.getSession().setAttribute("recap", recap);
		request.getRequestDispatcher("WEB-INF/Account.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
