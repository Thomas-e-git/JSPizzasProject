package lst;


import java.util.HashMap;
import java.util.Map;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;


import model.Article;

/**
 * Application Lifecycle Listener implementation class LstSession
 *
 */
@WebListener
public class LstSession implements HttpSessionListener {

	/**
	 * Default constructor.
	 */
	public LstSession() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
	 */
	public void sessionCreated(HttpSessionEvent arg0) {
		arg0.getSession().setAttribute("user", null);
		arg0.getSession().setAttribute("active", false);
		Map<Article, Integer> cart = new HashMap<Article, Integer>();
		arg0.getSession().setAttribute("cart", cart);
	}

	/**
	 * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
	 */
	public void sessionDestroyed(HttpSessionEvent arg0) {
		
	}

}
