package lst;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import dao.DaoArticle;
import model.Article;

/**
 * Application Lifecycle Listener implementation class LstApp
 *
 */
@WebListener
public class LstApp implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public LstApp() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0)  { 
         // TODO Auto-generated method stub
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0)  { 
    	DaoArticle daoArticle = new DaoArticle(); 
         try {
			ArrayList<Article> listePizzas = daoArticle.findAll();
			arg0.getServletContext().setAttribute("listePizzas", listePizzas);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
}
